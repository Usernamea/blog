#!/usr/bin/env stack
-- stack --silent --resolver lts-15.3 script

import Control.Monad.IO.Class

main :: IO ()
main = do
  let var = take 4 pow2s
  liftIO $ print var

pow2s = scanl (+) 1 pow2s  
