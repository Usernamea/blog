---
title: regex
latex: true
date: 2021-11-14
---

# regex

## regexp

*(* atom quantifier$^?$ *)* *  
*(* **`|`** *(* atom quantifier$^?$ *)* * *)* *

## atom

░░     **`(`** regexp **`)`**  
░░     *any character exept*     `. \ ? * + { } ( ) | [ ]`  
░░     **`.`**     (*this means* `[^\n\r]`)  
░░     **`\`** *one of*     `n r t . \ ? * + { } ( ) | [ ] _ ^`  
░░     **`\P`** *or* **`\p`** **`{`** more1 **`}`**  
░░     **`\`** *one of*     `s S i I c C d D w W a A`  
░░     charClassExpr  

### more1
`L` *opt one of*     `u l t m o`  

`M` *opt one of*     `n c l`  

`N` *opt one of*     `d l o`  

`P` *opt one of*     `c d s e i f o`  

`Z` *opt one of*     `s l p`  

`S` *opt one of*     `m c k o`  

`C` *opt one of*     `c f o n`  

`Is` normalized-block-name (e.g. "Latin-1Supplement")

## charClassExpr

`[` `^`$^?$ charGroupPart$^+$ character-class-substr$^?$ `]`

### character-class-substr
`[` `-` charClassExpr `]`

### something
every character exept `\ [ ]`  
`\` one of *[* `n r t . \ ? * + { } ( ) | [ ] _ ^` *]*

### charGroupPart
something  
something `-` something  
`\P` *or* `\p {` more1 `}`  
`\` one of     [ `s S i I c C d D w W a A` ]  

## sed

three digits: `[[:digit:]]\{3\}`
