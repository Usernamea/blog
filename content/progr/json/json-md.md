---
title: json
latex: true
date: 2021-11-14
---

# JSON

*[* object *or* array *]*$^+$

object:  
**\{** *[* string : value *[* , string : value *]*$^{ * }$ *]*$^?$ **\}**

array:  
**[** *[* value *[*, value *]*$^{ * }$ *]*$^?$ **]**

value:  
•  
string  
number  
**true**  
**false**  
**null**

number:  
int *[* . *[* 0 *to* 9 *]*$^?$
*[* *[* e *or* E *]* *[* + *or* - *]*$^?$
*[* 0 *to* 9 *]*+ *]*$^?$

int:  
*[* - *]*$^?$ *[* 0 *or* *[*
*[* 1 *to* 9 *]* *[* 0 *to* 9 *]* * *]* *]*

string:  
**"** *[* char *]* * **"**

char:  
*any Unicode character except* " *or* \\ *or* control-character

control-character:  
`\` *[* *one of* " `\` / b f n r t u*four-hex-digits* *]*

# jshon

https://github.com/keenerd/jshon

`jshon [`actions`]`

`-u`     unescape string  
`-t`     id type (str, object, list, number, bool, null)  
`-l`     length  
`-k`     keys (object)  

`-e` *index*     extract (object, array).  
`-i` *index* insert, opposite of extract, merges json up
the stack. objects will overwrite, arrays will insert. Arrays
can take negative numbers or 'append'  
`-d` *index*     remove an element from an objet or array. Negative array indexes will wrap around.  
`-a`     (across) maps the remaining actions across the selected element. Only
works on objects and arrays. Multiple -a calls can be nested, though the need
is rare in practice.  
`-s` *value*\quad load string (adds json escapes)  
`-n` *value*\quad load nonstring (`t f n [] {}`)  
`-p`     pop stack / undo last manipulation  
`-j`     json literal (preserves json escapes, display value)

`-F` *file*  
`-I`     in-place file editing (requires -F)  
`-S`     sort keys when writing objects  
`-Q`     quiet  
`-C`     continue through errors  
`-P`     detect and ignore JSONP wrapper, if present  
`-0`     null delimiters

Use *extract* (`-e`) to dive into json tree, *delete* (`-d`,)
*string* (`-s`), *nonstring* (`-n`) to change things, and
*insert* (`-i`) to push the changes back up the tree.

```json
{
 "a": 1,
 "b": [
  true,
  false,
  null,
  "none"
 ],
 "c": {
  "d": 4,
  "e": 5
 }
}

```

Most common read-only uses will only need several `-e` actions and one `-a`
in the middle of them.

`jshon -e "c" -e "d"`
``` json

```

`jshon -e "c" -e "d" -i "f" -d "d"`
```json

```

`jshon -e "c" -n 1 -i "f" -d "d"`
```json
{
 "e": 5,
 "f": 1
}
```

`jshon -e "c" -n 1 -i "f" -d "d" -i "c"`
```json
{
 "a": 1,
 "b": [
  true,
  false,
  null,
  "none"
 ],
 "c": {
  "e": 5,
  "f": 1
 }
}
```

`jshon -e b -d 0 -s foo -i 0`
```json
[
 "foo",
 false,
 null,
 "none"
]
```

`jshon -e b -a -t`
```text
bool
bool
null
string
```

`jshon -e c -e d -u -p -e e -u`
== `jshon -ec -ed -upee -u`

`jshon -n {} -s one -i vara`
```json
{
 "vara": "one"
}
```

# jq

`echo '["a", "b", "c", "d"]' | jq '.[1:3]'`

[
  "b",
  "c"
]

