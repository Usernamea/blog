---
title: "Setup debian"
date: 2021-10-24T18:47:45+02:00
---

download iso image
copy to dev/sdX (X = 'b', in my case)
install...
apt install sudo
su -l
adduser username sudo
log out log in

# install

## apt

fonts-mononoki
sway
foot
firefox-esr
curl

## script, download 

neovim
nvm (node, npm)
miniconda3

## conda

pip3 install poetry

# setup

setup config files (sway, foot, bashrc)
/sys/class/backlight/intel_backlight/brightness

## neovim

paq
sandwich
lighthouse (colorscheme)

# TODO

setup ssh for github
setup neovim
