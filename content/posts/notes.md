---
title: "Some Links"
date: 2020-06-23T11:26:53+02:00
---

https://gist.github.com/a-rodin/fef3f543412d6e1ec5b6cf55bf197d7b  
https://codersblock.com/blog/customizing-github-gists/

https://github.com/sheredom/hashmap.h

https://xlinux.nist.gov/dads/

https://github.com/TinyCC/TinyCC
gh/DaveGamble/cJSON

sketchpunk/funwithwebgl2

zeno.org
the-eye.eu
ncatlab.org

https://www.cs.kent.ac.uk/~djb/pgn-extract/pga-extract-21-02.tgz

https://wasmweekly.news

[Computer Graphics at TU Wien](https://www.youtube.com/channel/UCS9CFdjdEcq_NhaSFb_P-yA/videos)

https://github.com/gahjelle/decorators_tutorial/tree/master/2021

https://fastify-vite.dev/

https://github.com/terixjs/fastify-vite

https://reactjs.org/docs/rendering-elements.html

https://blog.sessionstack.com/how-javascript-works-memory-management-how-to-handle-4-common-memory-leaks-3f28b94cfbec

https://fullstackopen.com/en/part2/getting_data_from_server

https://wiki.mozilla.org/Gecko:Overview

https://firefox-source-docs.mozilla.org/browser/urlbar/overview.html

https://firefox-source-docs.mozilla.org/setup/linux_build.html

https://github.com/LizardLad/Raspi3-Kernel

https://bob.cs.sonoma.edu/IntroCompOrg-RPi/sec-using-book.html

https://fanglingsu.github.io/vimb/

https://www.django-rest-framework.org/

https://github.com/bytecodealliance/wasmtime/blob/main/docs/WASI-tutorial.md#running-common-languages-with-wasi

https://github.com/bytecodealliance/wasmtime/blob/main/docs/WASI-tutorial.md#running-common-languages-with-wasi

https://docs.wasmtime.dev/wasm-rust.html

https://www.youtube.com/channel/UCg6SQd5jnWo5Y70rZD9SQFA/videos

https://www.youtube.com/c/grafikart/videos

https://github.com/nshen/vite-plugin-wasm-pack

https://svelte.dev/tutorial/in-and-out
